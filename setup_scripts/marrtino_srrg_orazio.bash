#!/bin/bash

# First installation of srrg_orazio modules

CATKIN_DIR=${HOME}/catkin_ws

# install
sudo apt-get update
sudo apt-get install libreadline-dev libyaml-cpp-dev arduino-mk libncurses5-dev libwebsockets-dev libreadline-dev ros-${ROS_DISTRO}-turtlebot-* ros-kinetic-libfreenect ros-kinetic-openni*

# create folders
mkdir -p $CATKIN_DIR/src

# Update srrg
cd $CATKIN_DIR/src

git clone https://gitlab.com/srrg-software/srrg_cmake_modules.git
git clone https://gitlab.com/srrg-software/srrg_core.git
git clone https://gitlab.com/srrg-software/srrg_core_ros.git
git clone https://gitlab.com/srrg-software/srrg2_orazio_core.git
git clone https://gitlab.com/srrg-software/srrg2_orazio_ros.git
git clone https://github.com/robopeak/rplidar_ros.git
git clone https://bitbucket.org/iocchi/apriltags_ros.git
git clone https://bitbucket.org/iocchi/marrtino_apps.git
git clone https://bitbucket.org/iocchi/stage_environments.git
git clone https://github.com/Imperoli/gradient_based_navigation.git
git clone https://bitbucket.org/ggrisetti/thin_drivers.git
git clone https://bitbucket.org/ggrisetti/depth2laser.git

cd thin_drivers
rm -rf thin_visensor #per arm
rm -rf thin_xtion    #per arm

export OPENNI2_HOME=/usr/include/openni2

# set stable versions

# Dec 22, 2017
cd srrg_cmake_modules; git checkout a7b2a224686f1f2d674a2cd409d886e34d5a63aa; 
cd -

# Dec 18, 2017
cd srrg_core; git checkout 5a13a106f810452914b09b448bd41104ed98e30b; 
cd src
echo "add_subdirectory(srrg_boss)" > CMakeLists.txt 
echo "add_subdirectory(srrg_types)" >> CMakeLists.txt 
echo "add_subdirectory(srrg_system_utils)" >> CMakeLists.txt 
echo "add_subdirectory(srrg_image_utils)" >> CMakeLists.txt 
echo "add_subdirectory(srrg_messages)" >> CMakeLists.txt 
echo "add_subdirectory(srrg_path_map)" >> CMakeLists.txt 
echo "#add_subdirectory(examples)" >> CMakeLists.txt 
echo "#add_subdirectory(applications)" >> CMakeLists.txt 
cd ../..

# Dec 7, 2017
cd srrg_core_ros; git checkout c7d34432d5ba4638e0436d888c293937c6b4e85b; 
cd -

# Dec 29, 2017
cd srrg2_orazio_core; git checkout a190319fa9efd5274b13577c3abdf6b53dcca043; 
cd -

# Dec 28, 2017
cd srrg2_orazio_ros; git checkout 46d06bf4c6ab37dc41a7120824a047622513bae3; 
cd -


# ROS setup and compile
cd ../
sleep 5
catkin_make

# Create udev rules for RPLIDAR so that /dev/rplidar will be assigned to the sensor device
cd src/rplidar_ros/scripts
./create_udev_rules.sh

# MARRTINO_APPS_HOME
echo "export MARRTINO_APPS_HOME=$CATKIN_DIR/src/marrtino_apps" >> ~/.bashrc
