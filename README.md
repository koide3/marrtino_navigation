## marrtino_navitation

### Installation
```bash
cd marrtino_navigation
./setup_scripts/marrtino_srrg_orazio.bash
source ~/.bashrc

# change the permission to connect with marrtino
sudo chmod 666 /dev/ttyACM0
```

### Navigation

In case you need to perform mapping, add "mapping:=true" at the last of the command.

```bash
roslaunch marrtino_navigation robot_navigation.launch # mapping:=true
```

```bash
rosrun rosrun teleop_twist_keyboard teleop_twist_keyboard.py 
```

### Tips

If the LIDAR is rotated by 180 degree, change the sensor yaw angle on the line #188 in urdf/marrtino_robot.xacro.


### Compatibility

- gmapping_demo.launch

```xml
  <arg name="base_frame"  value="/tutlebot/base_footprint"/>
  <arg name="odom_frame"  value="/tutlebot//odom"/>
  <arg name="scan_topic"  value="/tutlebot//scan"/>
```
to
```xml
```

